import axios from 'axios'

export default {
    namespaced : true,
    state: {
        authenticated: false,
        user: [],
        token : '',
    },

    getters : {
        check(state) {
           return state.authenticated
        },

        user(state) {
            return state.user
        },

        token(state) {
            return state.token
        }        
    },
    
    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value            
        },
        SET_USER(state, value) {
            state.user = value            
        },

        SET_TOKEN(state, value)  {
            state.token = value            
        },
    },

    actions: {  
        SET_TOKEN ({commit, dispatch}, value) {
            commit('SET_TOKEN', value)
            dispatch('CHECKTOKEN', value)
        },

        async CHECKTOKEN({commit}, value) {
            let config = {
                method: 'post',
                url: 'https://demo-api-vue.sanbercloud.com/api/v2/auth/me',
                headers: {
                    'Authorization' : 'Bearer ' + value,
                },
            }

            await axios(config) 
                .then(response => {
                    commit('SET_AUTHENTICATED', true)
                    commit('SET_USER', response.data)
                })
                .catch( () => {
                    commit('SET_AUTHENTICATED', false)
                    commit('SET_USER', [])
                    commit('SET_TOKEN', '')
                });
        },  

        // async login({dispatch}, credentials) {
        //     await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/login', credentials)
        //     dispatch('me') 
        //     let response = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/login', credentials)
        //     console.log(response)
        //     const token = response.data.access_token;
        //     this.token = response.data.access_token;
        //     localStorage.setItem("token", token);
        // },

        // async me({ commit }) {            
        //     try {
        //         // let { data } = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/me')
        //         let response = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/me')
        //         commit('SET_AUTHENTICATED', true)
        //         commit('SET_USER', response.data)
        //         commit('SET_TOKEN', response.data.access_token)
        //         this.token = response.data.access_token
        //         console.log(response)
        //     } catch (e) {
        //         commit('SET_AUTHENTICATED', false)
        //         commit('SET_USER', [])
        //         commit('SET_TOKEN', '')
        //     }  
        // },

        async signout({commit}) {
            let config = {
                method: 'post',
                url : 's://demo-api-vue.sanbercloud.com/api/v2/auth/logout',
                headers: {
                    'Authorization' : 'Bearer ' + this.token,
                },
            }  
            await axios(config) 
            // await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/logout')
            .then(response => {
                commit('SET_AUTHENTICATED', false)
                commit('SET_USER', [])
                commit('SET_TOKEN', '')
                console.log(response)
            })
            .catch( (response) => {
               console.log(response)
            });
            
        },
    },
}